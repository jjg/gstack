Gstack documentation
--------------------

The docbook source in `gstack.xml` is used to generate the Unix
man-page `gstack.3` and the plain text `gstack.txt`. The tools 
`xsltproc` and `lynx` are required to regenerate these files.

The script `gstack-fetch.sh` pulls the latest version of `gstack.c`
and `gstack.h` from the GitHub master to the current directory.
