/*
  tests_constructor.h
  Copyright (c) J.J. Green 2014
*/

#ifndef TESTS_CONSTRUCTOR_H
#define TESTS_CONSTRUCTOR_H

#include <CUnit/CUnit.h>

extern CU_TestInfo tests_constructor[]; 

extern void test_constructor_new(void);
extern void test_constructor_size_zero(void);
extern void test_constructor_initial_zero(void);
extern void test_constructor_inc_zero(void);

#endif
