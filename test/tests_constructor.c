/*
  tests_constructor.h
  Copyright (c) J.J. Green 2014
*/

#include <gstack.h>
#include "tests_constructor.h"
#include "tests_helper.h"

CU_TestInfo tests_constructor[] = 
  {
    {"constructor",  test_constructor_new},
    {"size zero",    test_constructor_size_zero},
    {"initial zero", test_constructor_initial_zero},
    {"inc zero",     test_constructor_inc_zero},
    CU_TEST_INFO_NULL,
  };

extern void test_constructor_new(void)
{
  gstack_t* g;

  CU_TEST_FATAL( (g = gstack_new(4, 4, 4)) != NULL );
  CU_ASSERT( gstack_empty(g) == 1 );

  gstack_destroy(g);
}

extern void test_constructor_size_zero(void)
{
  gstack_t* g;

  CU_ASSERT( (g = gstack_new(0, 10, 10)) == NULL );
}

extern void test_constructor_initial_zero(void)
{
  gstack_t* g;
  int n = 3;
  
  CU_TEST_FATAL( (g = gstack_new(sizeof(int), 0, 10)) != NULL );
  CU_ASSERT( gstack_empty(g) == 1 );
  CU_ASSERT( gstack_push(g, &n) == 0 );
  CU_ASSERT( gstack_empty(g) == 0 );

  gstack_destroy(g);
}

extern void test_constructor_inc_zero(void)
{
  gstack_t* g;

  CU_ASSERT( (g = gstack_new(sizeof(int), 10, 0)) == NULL );
}
