/*
  tests_helper.h
  Copyright (c) J.J. Green 2015
*/

#ifndef TESTS_GSTACK_HELPER_H
#define TESTS_GSTACK_HELPER_H

#include <gstack.h>

extern gstack_t* build_gstack(int n);

#endif
